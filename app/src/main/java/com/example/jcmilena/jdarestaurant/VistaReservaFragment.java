package com.example.jcmilena.jdarestaurant;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class VistaReservaFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnReservaRecyclerListener mListener;

    RecyclerView reservas_recycler;
    List<Reserva> reservas = new ArrayList<>();
    ReservaAdapter reservaAdapter;

    public VistaReservaFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VistaReservaFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VistaReservaFragment newInstance(String param1, String param2) {
        VistaReservaFragment fragment = new VistaReservaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vista_reserva, container, false);


        reservas_recycler = view.findViewById(R.id.reservas_Recycler);
        reservas_recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        reservaAdapter = new ReservaAdapter(reservas);
        reservas_recycler.setAdapter(reservaAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL);
        reservas_recycler.addItemDecoration(dividerItemDecoration);

        return view;
    }


    public class ReservaViewHolder extends RecyclerView.ViewHolder{

        TextView fecha, personas;

        public ReservaViewHolder(@NonNull View itemView) {
            super(itemView);

            fecha = itemView.findViewById(R.id.fechaViewHolder);
            personas = itemView.findViewById(R.id.personasViewHolder);
            
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    
                }
            });
        }
    }


    public class ReservaAdapter extends Adapter<ReservaViewHolder>{

        List<Reserva> reservaList;

        public ReservaAdapter(List<Reserva> reservaList) {
            this.reservaList = reservaList;
        }

        @NonNull
        @Override
        public ReservaViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View itemview = getLayoutInflater().inflate(R.layout.reserva_viewholder, viewGroup, false);
            return new ReservaViewHolder(itemview);
        }

        @Override
        public void onBindViewHolder(@NonNull ReservaViewHolder reservaViewHolder, int i) {

            reservaViewHolder.fecha.setText("0");
            reservaViewHolder.personas.setText("0");

        }

        @Override
        public int getItemCount() {
            return 0;
        }
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnReservaRecyclerListener) {
            mListener = (OnReservaRecyclerListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnReservaRecyclerListener {

    }
}
